import Vue from 'vue'
import Router from 'vue-router'
import ProductList from './components/ProductList.vue'
import ProductDetails from './components/ProductDetails.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ProductList',
      component: ProductList
    },
    {
      path: '/product-details/:id',
      name: 'ProductDetails',
      component: ProductDetails
    },
  ]
})
