import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state() {
    return {
      productList: [],
      productId: '',
      productDetails: {},
      isFetchingFromProductDetails: 'none',
      newProductDetails: '',
      getIndex: null
    }
  },
  mutations: {
    setProductList(state, productList) {
      state.productList = productList;
    },
    setProductData(state, payload) {
      state.productId = payload.value;
    },
    fetchDataDetails(state, payload) {
      state.isFetchingFromProductDetails = 'yes';
      state.newProductDetails = payload;
    },
    editDataTitle(state,titleData) {
      state.storeProductTitle = titleData;
      state.editTitle = true;
    },
    getProductData(state) {
      let arr = state.productList;
      arr.map(product => {
        if ( state.productId === product.productId ) {
          state.productDetails = state.productList[state.getIndex];
        }
      })
    },
    getIndex(state,index){
      state.getIndex = index.value;
    }
  },
  actions: {
    async getProductList(context) {
      await axios.get('/products.json')
      .then(response => {
        context.commit('setProductList',response.data)
      })
    },
    getNewDataDetails(context) {
      context.state.productList.map((newProduct,index) => {
        if ( context.state.productId === newProduct.productId ) {
          context.state.getIndex = index;
          context.state.productList[context.state.getIndex] = context.state.productDetails;
          let result = context.state.productList;
          context.state.productList = result;
        }
      })
    }
  },
  getters: {
    productListData(state) {
      return state.productList
    }
  }
})
